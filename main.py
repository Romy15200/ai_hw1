from framework import *
from problems import *

from matplotlib import pyplot as plt
import numpy as np
from typing import List, Union, Optional

# Load the streets map
streets_map = StreetsMap.load_from_csv(Consts.get_data_file_path("tlv_streets_map.csv"))

# Make sure that the whole execution is deterministic.
# This is important, because we expect to get the exact same results
# in each execution.
Consts.set_seed()


def plot_distance_and_expanded_wrt_weight_figure(
        problem_name: str,
        weights: Union[np.ndarray, List[float]],
        total_cost: Union[np.ndarray, List[float]],
        total_nr_expanded: Union[np.ndarray, List[int]]):
    """
    Use `matplotlib` to generate a figure of the distance & #expanded-nodes
     w.r.t. the weight.
    TODO [Ex.15]: Complete the implementation of this method.
    """
    weights, total_cost, total_nr_expanded = np.array(weights), np.array(total_cost), np.array(total_nr_expanded)
    assert len(weights) == len(total_cost) == len(total_nr_expanded)
    assert len(weights) > 0
    is_sorted = lambda a: np.all(a[:-1] <= a[1:])
    assert is_sorted(weights)

    fig, ax1 = plt.subplots()

    p1, = ax1.plot(weights, total_cost, color='b', label='Solution cost', linestyle='-')  # TODO: pass the relevant params instead of `...`.

    # ax1: Make the y-axis label, ticks and tick labels match the line color.
    ax1.set_ylabel('Solution cost', color='b')
    ax1.tick_params('y', colors='b')
    ax1.set_xlabel('weight')

    # Create another axis for the #expanded curve.
    ax2 = ax1.twinx()

    p2, = ax2.plot(weights, total_nr_expanded, color='r', label='#Expanded states', linestyle='-' )  # TODO: pass the relevant params instead of `...`.

    # ax2: Make the y-axis label, ticks and tick labels match the line color.
    ax2.set_ylabel('#Expanded states', color='r')
    ax2.tick_params('y', colors='r')

    curves = [p1, p2]
    ax1.legend(curves, [curve.get_label() for curve in curves])

    fig.tight_layout()
    plt.title(f'Quality vs. time for wA* \non problem {problem_name}')
    plt.show()


def run_astar_for_weights_in_range(heuristic_type: HeuristicFunctionType, problem: GraphProblem, n: int = 30,
                                   max_nr_states_to_expand: Optional[int] = 40_000,
                                   low_heuristic_weight: float = 0.5, high_heuristic_weight: float = 0.95):

    range_weights = np.linspace(low_heuristic_weight, high_heuristic_weight, n)
    costs = []
    nr_expanded = []
    weights = []
    for weight in range_weights:
        astar_w = AStar(heuristic_type, weight, max_nr_states_to_expand)
        res_w = astar_w.solve_problem(problem)
        if res_w.is_solution_found:
            costs.append(res_w.solution_g_cost)
            nr_expanded.append(res_w.nr_expanded_states)
            weights.append(weight)
    plot_distance_and_expanded_wrt_weight_figure(problem.name,weights,costs,nr_expanded)




# --------------------------------------------------------------------
# ------------------------ StreetsMap Problem ------------------------
# --------------------------------------------------------------------

def toy_map_problem_experiments():
    print()
    print('Solve the map problem.')

    toy_map_problem = MapProblem(streets_map, 54, 549)
    uc = UniformCost()
    res_uc = uc.solve_problem(toy_map_problem)
    print(res_uc)

    aStar_Null = AStar(NullHeuristic)
    res_Null = aStar_Null.solve_problem(toy_map_problem)
    print(res_Null)

    aStar_Dist = AStar(AirDistHeuristic)
    res_Dist = aStar_Dist.solve_problem(toy_map_problem)
    print(res_Dist)

    run_astar_for_weights_in_range(AirDistHeuristic, toy_map_problem)


# --------------------------------------------------------------------
# ---------------------------- MDA Problem ---------------------------
# --------------------------------------------------------------------

loaded_problem_inputs_by_size = {}
loaded_problems_by_size_and_opt_obj = {}


def get_mda_problem(
        problem_input_size: str = 'small',
        optimization_objective: MDAOptimizationObjective = MDAOptimizationObjective.Distance):
    if (problem_input_size, optimization_objective) in loaded_problems_by_size_and_opt_obj:
        return loaded_problems_by_size_and_opt_obj[(problem_input_size, optimization_objective)]
    assert problem_input_size in {'small', 'moderate', 'big'}
    if problem_input_size not in loaded_problem_inputs_by_size:
        loaded_problem_inputs_by_size[problem_input_size] = MDAProblemInput.load_from_file(
            f'{problem_input_size}_mda.in', streets_map)
    problem = MDAProblem(
        problem_input=loaded_problem_inputs_by_size[problem_input_size],
        streets_map=streets_map,
        optimization_objective=optimization_objective)
    loaded_problems_by_size_and_opt_obj[(problem_input_size, optimization_objective)] = problem
    return problem


def basic_mda_problem_experiments():
    print()
    print('Solve the MDA problem (small input, only distance objective, UniformCost).')

    small_mda_problem_with_distance_cost = get_mda_problem('small', MDAOptimizationObjective.Distance)

    uc = UniformCost()
    res_uc_mda = uc.solve_problem(small_mda_problem_with_distance_cost)
    print(res_uc_mda)



def mda_problem_with_astar_experiments():
    print()
    print('Solve the MDA problem (moderate input, only distance objective, A*, '
          'MaxAirDist & SumAirDist & MSTAirDist heuristics).')

    moderate_mda_problem_with_distance_cost = get_mda_problem('moderate', MDAOptimizationObjective.Distance)

    aStar_max = AStar(MDAMaxAirDistHeuristic)
    res_aStar_max_mda = aStar_max.solve_problem(moderate_mda_problem_with_distance_cost)
    print(res_aStar_max_mda)

    aStar_sum = AStar(MDASumAirDistHeuristic)
    res_aStar_sum_mda = aStar_sum.solve_problem(moderate_mda_problem_with_distance_cost)
    print(res_aStar_sum_mda)

    aStar_mst = AStar(MDAMSTAirDistHeuristic)
    res_aStar_mst_mda = aStar_mst.solve_problem(moderate_mda_problem_with_distance_cost)
    print(res_aStar_mst_mda)


def mda_problem_with_weighted_astar_experiments():
    print()
    print('Solve the MDA problem (small & moderate input, only distance objective, wA*).')

    small_mda_problem_with_distance_cost = get_mda_problem('small', MDAOptimizationObjective.Distance)
    moderate_mda_problem_with_distance_cost = get_mda_problem('moderate', MDAOptimizationObjective.Distance)

    run_astar_for_weights_in_range(MDAMSTAirDistHeuristic, small_mda_problem_with_distance_cost)
    run_astar_for_weights_in_range(MDASumAirDistHeuristic, moderate_mda_problem_with_distance_cost)


def monetary_cost_objectives_mda_problem_experiments():
    print()
    print('Solve the MDA problem (monetary objectives).')

    small_mda_problem_with_monetary_cost = get_mda_problem('small', MDAOptimizationObjective.Monetary)
    moderate_mda_problem_with_monetary_cost = get_mda_problem('moderate', MDAOptimizationObjective.Monetary)

    uc2 = UniformCost()
    res_mda_monetary_small = uc2.solve_problem(small_mda_problem_with_monetary_cost)
    print(res_mda_monetary_small)

    uc3 = UniformCost()
    res_mda_monetary_moderate=uc3.solve_problem(moderate_mda_problem_with_monetary_cost)
    print(res_mda_monetary_moderate)


def multiple_objectives_mda_problem_experiments():
    print()
    print('Solve the MDA problem (moderate input, distance & tests-travel-distance objectives).')

    moderate_mda_problem_with_distance_cost = get_mda_problem('moderate', MDAOptimizationObjective.Distance)
    moderate_mda_problem_with_tests_travel_dist_cost = get_mda_problem('moderate', MDAOptimizationObjective.
                                                                       TestsTravelDistance)

    aStar_mda_travel_heuristic = AStar(MDATestsTravelDistToNearestLabHeuristic)
    res_mda_travel = aStar_mda_travel_heuristic.solve_problem(moderate_mda_problem_with_tests_travel_dist_cost)
    print(res_mda_travel)

    aStar_mst = AStar(MDAMSTAirDistHeuristic)
    optimal_sol = aStar_mst.solve_problem(moderate_mda_problem_with_distance_cost)
    optimal_distance_cost = optimal_sol.solution_cost.get_g_cost()
    epsilon = 0.6
    max_distance_cost = optimal_distance_cost*(1+epsilon)
    aStar_for_a2 = AStar(MDATestsTravelDistToNearestLabHeuristic, open_criterion= \
                             lambda search_node: search_node.cost.distance_cost <= max_distance_cost)
    res = aStar_for_a2.solve_problem(moderate_mda_problem_with_tests_travel_dist_cost)
    print(res)



def mda_problem_with_astar_epsilon_experiments():
    print()
    print('Solve the MDA problem (small input, distance objective, using A*eps, use non-acceptable '
          'heuristic as focal heuristic).')

    small_mda_problem_with_distance_cost = get_mda_problem('small', MDAOptimizationObjective.Distance)

    # Firstly solve the problem with AStar & MST heuristic for having a reference for #devs.
    astar = AStar(MDAMSTAirDistHeuristic)
    res = astar.solve_problem(small_mda_problem_with_distance_cost)
    print(res)

    def within_focal_h_sum_priority_function(node: SearchNode, problem: GraphProblem, solver: AStarEpsilon):
        if not hasattr(solver, '__focal_heuristic'):
            setattr(solver, '__focal_heuristic', MDASumAirDistHeuristic(problem=problem))
        focal_heuristic = getattr(solver, '__focal_heuristic')
        return focal_heuristic.estimate(node.state)

    aStarEpsilon = AStarEpsilon(MDAMSTAirDistHeuristic, focal_epsilon=0.23, max_focal_size=40,
                                within_focal_priority_function=within_focal_h_sum_priority_function)
    res = aStarEpsilon.solve_problem(small_mda_problem_with_distance_cost)
    print(res)


def mda_problem_anytime_astar_experiments():
    print()
    print('Solve the MDA problem (moderate input, only distance objective, Anytime-A*, '
          'MSTAirDist heuristics).')

    moderate_mda_problem_with_distance_cost = get_mda_problem('moderate', MDAOptimizationObjective.Distance)

    anytimeAStar = AnytimeAStar(MDAMSTAirDistHeuristic,1000)
    res_anytimeAStar = anytimeAStar.solve_problem(moderate_mda_problem_with_distance_cost)
    print(res_anytimeAStar)


def run_all_experiments():
    print('Running all experiments')
    toy_map_problem_experiments()
    basic_mda_problem_experiments()
    mda_problem_with_astar_experiments()
    mda_problem_with_weighted_astar_experiments()
    monetary_cost_objectives_mda_problem_experiments()
    multiple_objectives_mda_problem_experiments()
    mda_problem_with_astar_epsilon_experiments()
    mda_problem_anytime_astar_experiments()


if __name__ == '__main__':
    run_all_experiments()
