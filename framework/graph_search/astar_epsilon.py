from .graph_problem_interface import *
from .astar import AStar
from typing import Optional, Callable
import numpy as np
import math


class AStarEpsilon(AStar):
    """
    This class implements the (weighted) A*Epsilon search algorithm.
    A*Epsilon algorithm basically works like the A* algorithm, but with
    another way to choose the next node to expand from the open queue.
    """

    solver_name = 'A*eps'

    def __init__(self,
                 heuristic_function_type: HeuristicFunctionType,
                 within_focal_priority_function: Callable[[SearchNode, GraphProblem, 'AStarEpsilon'], float],
                 heuristic_weight: float = 0.5,
                 max_nr_states_to_expand: Optional[int] = None,
                 focal_epsilon: float = 0.1,
                 max_focal_size: Optional[int] = None):
        # A* is a graph search algorithm. Hence, we use close set.
        super(AStarEpsilon, self).__init__(heuristic_function_type, heuristic_weight,
                                           max_nr_states_to_expand=max_nr_states_to_expand)
        self.focal_epsilon = focal_epsilon
        if focal_epsilon < 0:
            raise ValueError(f'The argument `focal_epsilon` for A*eps should be >= 0; '
                             f'given focal_epsilon={focal_epsilon}.')
        self.within_focal_priority_function = within_focal_priority_function
        self.max_focal_size = max_focal_size

    def _init_solver(self, problem):
        super(AStarEpsilon, self)._init_solver(problem)

    def _extract_next_search_node_to_expand(self, problem: GraphProblem) -> Optional[SearchNode]:
        """
        Extracts the next node to expand from the open queue,
         by focusing on the current FOCAL and choosing the node
         with the best within_focal_priority from it.
        """
        if self.open.is_empty():
            return None
        min_value = self.open.peek_next_node().expanding_priority
        max_focal_expanding_priority = min_value * (1 + self.focal_epsilon)
        focal_list = []
        i = self.max_focal_size
        while True:
            if self.open.is_empty() or self.open.peek_next_node().expanding_priority > max_focal_expanding_priority \
                    or (i is not None and i == 0):
                break

            focal_list.append(self.open.pop_next_node())
            if i is not None:
                i -= 1

        next_to_expand = min(focal_list, key=lambda node: self.within_focal_priority_function(node, problem, self))
        focal_list.remove(next_to_expand)
        for node in focal_list:
            self.open.push_node(node)

        if self.use_close:
            self.close.add_node(next_to_expand)
        return next_to_expand
